/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chairs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import java.io.*;
import java.util.Iterator;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;

/**
 * FXML Controller class
 *
 * @author michael
 */
public class ViewTransactionController implements Initializable {

    @FXML
    private TextArea viewTransactionArea;
    @FXML
    private Button viewTransactionBackButton;
    @FXML
    private Button reportButton;
    @FXML
    private MenuItem viewTransactionClose;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       
       viewTransactionArea.appendText("Date:                "+ ViewAllTransactionsController.selectedRecord.getDate()+"\n \n \n");
       viewTransactionArea.appendText("Customer Name:       "+ ViewAllTransactionsController.selectedRecord.getCustomerName()+"\n \n");
       viewTransactionArea.appendText("Number of Chairs:    "+ ViewAllTransactionsController.selectedRecord.getNumChairs()+"\n \n");
       viewTransactionArea.appendText("Number of Tables:    "+ ViewAllTransactionsController.selectedRecord.getNumTables()+"\n \n");
       viewTransactionArea.appendText("Number of Canopies:  "+ ViewAllTransactionsController.selectedRecord.getNumCanopies()+"\n \n");
       viewTransactionArea.appendText("Total Number hired:  "+ ViewAllTransactionsController.selectedRecord.getTotalCost()+"\n \n");
       
      // viewTransactionArea.setEditable(false);
    }    

    @FXML
    private void backToViewAll(ActionEvent event) {
        try {
        Parent addNewTransactionPage = FXMLLoader.load(getClass().getResource("ViewAllTransactions.fxml"));
        
        Scene scene = new Scene(addNewTransactionPage);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show(); 
            } catch(Exception e) {
               e.printStackTrace();
              }
    }

    @FXML
    private void printRecord(ActionEvent event) {
        String customerName = ViewAllTransactionsController.selectedRecord.getCustomerName(); //Get customer name to use as name of written file
        
        ObservableList<CharSequence> record = viewTransactionArea.getParagraphs(); //Get all text from text area box
        Iterator<CharSequence> it = record.iterator(); //For iterating over test area
        try{
            FileWriter wr = null;
            BufferedWriter bw = null;
            //File file = new File("/Users/michael/NetBeansProjects/Chairs/customer_details_record");
            
             bw = new BufferedWriter(new FileWriter(new File(customerName+"_record.txt")));
             while(it.hasNext()){
                 CharSequence sequence = it.next();
                 bw.append(sequence);
                 bw.newLine();
             }
             bw.flush();
             bw.close();
        } catch (Exception e){
            e.printStackTrace();
        }
        
        Alert a= new Alert(Alert.AlertType.INFORMATION);
        a.setContentText("Record printed! Check project directory");
        a.show();
     }

    @FXML
    private void exit(ActionEvent event) {
        System.exit(0);
    }
    
}
