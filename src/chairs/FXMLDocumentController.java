/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chairs;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author michael saneke
 */
public class FXMLDocumentController implements Initializable {

    @FXML
    private Button AddTransactionRecordButton;
    @FXML
    private Button viewTransactionRecordsButton;
    @FXML
    private Button mainExitButton;
    @FXML
    private MenuItem menuCloseButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void toAddTransactionRecord(ActionEvent event) { 
        
      try {
        Parent addNewTransactionPage = FXMLLoader.load(getClass().getResource("AddNewTransaction.fxml"));
        
        Scene scene = new Scene(addNewTransactionPage);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show(); 
            } catch(Exception e) {
               e.printStackTrace();
              }
      
        }


    @FXML
    private void toViewTransactionRecords(ActionEvent event) {
        try {
        Parent addNewTransactionPage = FXMLLoader.load(getClass().getResource("ViewAllTransactions.fxml"));
        
        Scene scene = new Scene(addNewTransactionPage);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show(); 
            } catch(Exception e) {
               e.printStackTrace();
              }
    }
    
    @FXML
    private void exitChairs(ActionEvent event) {
        System.exit(0);
    }

    private void viewAbout(ActionEvent event) {
        try {
        Parent addNewTransactionPage = FXMLLoader.load(getClass().getResource("About.fxml"));
        
        Scene scene = new Scene(addNewTransactionPage);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show(); 
            } catch(Exception e) {
               e.printStackTrace();
              }
    }
    
}
