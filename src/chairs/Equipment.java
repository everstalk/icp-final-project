/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chairs;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author michael saneke
 * 
 * Table view properties for view all transaction records
 */
class Equipment {
    
    public Equipment(String customerName, int chairsNum, int tablesNum, int canopiesNum, int totalCost, String date) {
        this.customerColumn = new SimpleStringProperty(customerName);
        this.chairsColumn = new SimpleIntegerProperty(chairsNum);
        this.tablesColumn= new SimpleIntegerProperty(tablesNum);
        this.canopiesColumn= new SimpleIntegerProperty(canopiesNum);
        this.totalCostColumn= new SimpleIntegerProperty(totalCost);
        this.dateColumn= new SimpleStringProperty(date);
    }
    
    
    private final StringProperty customerColumn;
    private final IntegerProperty chairsColumn;  
    private final IntegerProperty tablesColumn;
    private final IntegerProperty canopiesColumn;
    private final IntegerProperty totalCostColumn; 
    private final StringProperty dateColumn;
  
    
    public void setCustomerName(String val){
        customerColumn.set(val);
    }
    public String getCustomerName(){
        return customerColumn.get();
    }
    public StringProperty customerNameProp(){
        return customerColumn;
    }
    
    
    public void setNumChairs(int val){
        chairsColumn.set(val);
    }
    public int getNumChairs(){
        return chairsColumn.get();
    }
    public IntegerProperty numChairsProp(){
        return chairsColumn;
    }
    
    
    public void setNumTables(int val){
        tablesColumn.set(val);
    }
    public int getNumTables(){
        return tablesColumn.get();
    }
    public IntegerProperty numTablesProp(){
        return tablesColumn;
    }
    
    
    public void setNumCanopies(int val){
        canopiesColumn.set(val);
    }
    public int getNumCanopies(){
        return canopiesColumn.get();
    }
    public IntegerProperty numCanopiesProp(){
        return canopiesColumn;
    }
    
    
    public void setTotalCost(int val){
        totalCostColumn.set(val);
    }
    public int getTotalCost(){
        return totalCostColumn.get();
    }
    public IntegerProperty totalCostProp(){
        return totalCostColumn;
    }
    
    
    public void setDate(String val){
        dateColumn.set(val);
    }
    public String getDate(){
        return dateColumn.get();
    }
    public StringProperty dateProp(){
        return dateColumn;
    }
    
    
    
}
