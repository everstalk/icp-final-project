/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chairs;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;

/**
 * FXML Controller class
 *
 * @author michael saneke
 */
public class AddNewTransactionController implements Initializable {

    @FXML
    private Label label;
    @FXML
    private MenuBar addNTMenu;
    @FXML
    private TextField dateField;
    @FXML
    private TextField chairsQuantityField;
    @FXML
    private TextField tablesQuantityField;
    @FXML
    private TextField canopiesField;
    @FXML
    private Button backToMainButton;
    @FXML
    private Button saveTransactionButton;
    @FXML
    private TextField firstnameField;
    @FXML
    private TextField lastnameField;
    
    Connection connection = null;
    @FXML
    private TextField addressField;
    @FXML
    private TextField telephoneField;
    @FXML
    private MenuItem addTransactionCloseButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            connection = java.sql.DriverManager.getConnection(
                 "jdbc:mysql://localhost:3306/mysql?user=root&password=root");
        } catch (Exception e) {
            System.out.println(e);
            System.exit(0);
        }
    }

    /**
     * Go back to main window
     */

    @FXML
    private void backToStart(ActionEvent event) {
        try {
        Parent addNewTransactionPage = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(addNewTransactionPage);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show(); 
            } catch(Exception e) {
               e.printStackTrace();
              }
    }

    /*
    * Saving Transaction in database.
    * Using Regex pattern for input validation
    */
    @FXML
    private void saveInDatabase(ActionEvent event) {
        boolean valid = true;
        boolean allinput = true;
        try {
            if(firstnameField.getText().matches("^[a-zA-Z ]*$")){
            firstnameField.setText(firstnameField.getText());
            }
            else{
                valid = false;
            firstnameField.clear();
            }
            
            if(lastnameField.getText().matches("^[a-zA-Z ]*$")){
            lastnameField.setText(lastnameField.getText());
            }
            else{
                valid = false;
            firstnameField.clear();
            }
            
                     //Insert into database if input validation successful
                        if(valid){
                        PreparedStatement p = connection.prepareStatement
                    ("Insert Into transaction_details set firstname=?, lastname=?,telephone =?, address=?, event_time=?,chairs=?, tables=?, canopies=? ");
                        p.setString(1, firstnameField.getText());
                        p.setString(2, lastnameField.getText());
                        p.setString(3, telephoneField.getText());
                        p.setString(4, addressField.getText());
                        p.setString(5, dateField.getText());
                        p.setInt(6, Integer.parseInt(chairsQuantityField.getText()));
                        p.setInt(7, Integer.parseInt(tablesQuantityField.getText()));
                        p.setInt(8, Integer.parseInt(canopiesField.getText()));
                        
                        p.execute(); 
                        }
                        else{
                            Alert a= new Alert(Alert.AlertType.INFORMATION);
                             a.setContentText("Invalid text");
                            a.show();
                            
                        }
                        
                        
                    } catch (Exception e) {
                        e.printStackTrace();
                        allinput = false;
                    }
        
        
        
        firstnameField.clear();
        lastnameField.clear();
        telephoneField.clear();
        addressField.clear();
        dateField.clear();
        chairsQuantityField.clear();
        tablesQuantityField.clear();
        canopiesField.clear();
        
        if(!allinput){
            Alert a= new Alert(Alert.AlertType.INFORMATION);
            a.setContentText("Invalid Input : Some fields were empty");
            a.show();
        }
        
        else if(valid && allinput){
        Alert a= new Alert(Alert.AlertType.INFORMATION);
        a.setContentText("Transaction saved");
        a.show();
    }
    }
    
    /**
     * Close application from menu bar.
     */
    @FXML
    private void close(ActionEvent event) {
        System.exit(0);
    }
}