/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  michael
 * Created: Apr 16, 2017
 */

create database transaction_data;

use transaction_data;

create table transaction_details(

	transaction_id int NOT NULL  auto_increment,
    firstName varchar(30),
    lastName varchar(30),
    telephone varchar(15),
    address varchar(50),
    event_time varchar(10),
    chairs int(5),
    tables int(5),
    canopies int(5),
    primary key(transaction_id)
    
);

