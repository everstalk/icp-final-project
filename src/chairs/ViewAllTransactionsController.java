/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chairs;

import java.net.URL;
import java.sql.Connection;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Alert;
import javafx.scene.control.MenuItem;

/**
 * FXML Controller class
 *
 * @author michael
 */
public class ViewAllTransactionsController implements Initializable {

    @FXML
    private TableView<Equipment> transactionTable;
    @FXML
    private TableColumn<Equipment, String> customerColumn;
    @FXML
    private TableColumn<Equipment, Integer> chairsColumn;
    @FXML
    private TableColumn<Equipment, Integer> tablesColumn;
    @FXML
    private TableColumn<Equipment, Integer> canopiesColumn;
    @FXML
    private TableColumn<Equipment, Integer> totalCostColumn;
    @FXML
    private TableColumn<Equipment, String> dateColumn;
    @FXML
    private TextField searchRecordsField;
    @FXML
    private Button searchButtonField;
    @FXML
    private TextArea searchTextArea;
    @FXML
    private Button ViewTransactionsBackButton;
    
    Connection connection;
    @FXML
    private Button viewRecordButton;
    
    //public static ObservableList<Equipment> selectedRecord;
    public static Equipment selectedRecord;
    @FXML
    private MenuItem viewAllTransactionButton;


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        customerColumn.setCellValueFactory(cellData-> cellData.getValue().customerNameProp());
        chairsColumn.setCellValueFactory(cellData-> cellData.getValue().numChairsProp().asObject());
        tablesColumn.setCellValueFactory(cellData-> cellData.getValue().numTablesProp().asObject() );
        canopiesColumn.setCellValueFactory(cellData-> cellData.getValue().numCanopiesProp().asObject() );
        totalCostColumn.setCellValueFactory(cellData-> cellData.getValue().totalCostProp().asObject() );
        dateColumn.setCellValueFactory(cellData-> cellData.getValue().dateProp());
        transactionTable.setItems(loadData());
    }    

    @FXML
    private void backToMain(ActionEvent event) {
        try {
        Parent addNewTransactionPage = FXMLLoader.load(getClass().getResource("FXMLDocument.fxml"));
        
        Scene scene = new Scene(addNewTransactionPage);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show(); 
            } catch(Exception e) {
               e.printStackTrace();
              }
    }
    
     public ObservableList<Equipment> loadData() {
        ObservableList<Equipment> data = FXCollections.observableArrayList();
        
        try{
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            connection = java.sql.DriverManager.getConnection(
                 "jdbc:mysql://localhost:3306/mysql?user=root&password=root");
            Statement statement = connection.createStatement( );
            String SQL = "SELECT * FROM transaction_details";
            ResultSet result = statement.executeQuery( SQL );
            while(result.next()){
                String last = result.getString("lastname");
                String first = result.getString("firstname");
                String fullname = first + " " + last;
                int numChairs = result.getInt("chairs");
                int numTables = result.getInt("tables");
                int numCanopies = result.getInt("canopies");
                int numTotal = numChairs + numTables + numCanopies;
                String date = result.getString("event_time");
                data.add(new Equipment(fullname, numChairs, numTables, numCanopies, numTotal, date));       
                
            }
        
        }catch(Exception e){
            System.out.println(e);
        
        }
        return data;
        
}

    @FXML
    private void searchRecord(ActionEvent event) {
        
        searchTextArea.clear();
        
        String searchQuery = searchRecordsField.getText();
        try{
            
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            connection = java.sql.DriverManager.getConnection(
                 "jdbc:mysql://localhost:3306/mysql?user=root&password=root");
            Statement statement = connection.createStatement( );
            
            
            String SQL = "SELECT * FROM transaction_details WHERE telephone LIKE   concat('%' , '" + searchQuery + "','%') OR firstname  LIKE   concat('%' , '" + searchQuery + "','%') OR lastname LIKE concat('%' , '" + searchQuery + "','%')";

            ResultSet result = statement.executeQuery( SQL );
            
            
            
            while(result.next()){
                String first = result.getString("firstname");
                String last = result.getString("lastname");
                int numChairs = result.getInt("chairs");
                int numTables = result.getInt("tables");
                int numCanopies = result.getInt("canopies");
                String date = result.getString("event_time");
                String telephone = result.getString("telephone");
                
                
                searchTextArea.appendText(first + ",      " + last + ",       " + numChairs + ",      " + numTables + ",      " + numCanopies+ ",      "+ telephone+",      " + date+ "\n \n");       
                
                searchTextArea.setEditable(false);
            }
       
        
        }catch(Exception e){
            System.out.println(e);
        
        }
    }
    
    /*
    *Edit Record code starts here:
    */

    @FXML
    private void viewRecord(ActionEvent event) {
        
        selectedRecord = transactionTable.getSelectionModel().getSelectedItem();
        
        try {
        Parent addNewTransactionPage = FXMLLoader.load(getClass().getResource("ViewTransaction.fxml"));
        
        Scene scene = new Scene(addNewTransactionPage);
        Stage stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.show(); 
            } catch(Exception e) {
               e.printStackTrace();
              }
        
    }
     

    @FXML
    private void exit(ActionEvent event) {
        System.exit(0);
    }

    
}
